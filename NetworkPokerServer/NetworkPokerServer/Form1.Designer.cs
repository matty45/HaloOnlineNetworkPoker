﻿namespace NetworkPokerServer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textport = new System.Windows.Forms.TextBox();
            this.texthost = new System.Windows.Forms.TextBox();
            this.boxStart = new System.Windows.Forms.Button();
            this.boxStop = new System.Windows.Forms.Button();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textport
            // 
            this.textport.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textport.Location = new System.Drawing.Point(118, 12);
            this.textport.Name = "textport";
            this.textport.Size = new System.Drawing.Size(100, 22);
            this.textport.TabIndex = 0;
            this.textport.Text = "port";
            // 
            // texthost
            // 
            this.texthost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.texthost.Location = new System.Drawing.Point(12, 12);
            this.texthost.Name = "texthost";
            this.texthost.Size = new System.Drawing.Size(100, 22);
            this.texthost.TabIndex = 1;
            this.texthost.Text = "ip";
            // 
            // boxStart
            // 
            this.boxStart.Location = new System.Drawing.Point(224, 11);
            this.boxStart.Name = "boxStart";
            this.boxStart.Size = new System.Drawing.Size(75, 23);
            this.boxStart.TabIndex = 2;
            this.boxStart.Text = "Start";
            this.boxStart.UseVisualStyleBackColor = true;
            // 
            // boxStop
            // 
            this.boxStop.Location = new System.Drawing.Point(305, 11);
            this.boxStop.Name = "boxStop";
            this.boxStop.Size = new System.Drawing.Size(75, 23);
            this.boxStop.TabIndex = 3;
            this.boxStop.Text = "Stop";
            this.boxStop.UseVisualStyleBackColor = true;
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(12, 40);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(537, 201);
            this.txtStatus.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 253);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.boxStop);
            this.Controls.Add(this.boxStart);
            this.Controls.Add(this.texthost);
            this.Controls.Add(this.textport);
            this.MaximumSize = new System.Drawing.Size(579, 300);
            this.MinimumSize = new System.Drawing.Size(579, 300);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Halo Online Network Poker Server";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textport;
        private System.Windows.Forms.TextBox texthost;
        private System.Windows.Forms.Button boxStart;
        private System.Windows.Forms.Button boxStop;
        private System.Windows.Forms.TextBox txtStatus;
    }
}

